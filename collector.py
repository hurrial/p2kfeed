import feedparser,time,json

print("Script started.")


if __name__ == "__main__":

	previous_retrieved_ids = []

	while True:
		feed = feedparser.parse("http://feeds.livep2000.nl/?d=2")
		newly_retrieved_feeds = [f for f in feed["entries"] if f["id"] not in previous_retrieved_ids]

		if newly_retrieved_feeds:
			print(len(newly_retrieved_feeds), "new feeds.")
			with open("p2k_feeds.json", "a") as fw:
				for fd in newly_retrieved_feeds:
					fw.write(json.dumps(fd))
					fw.write("\n")
		                
		        # print number of the unique to console.
		else:
			print("0 new feed.")

		previous_retrieved_ids = [f["id"] for f in feed["entries"]]
		time.sleep(300)
		print("*"*50)
else:
	print("This runs only as standalone script.")


